import random
from datetime import datetime

ISO_DATE_FORMAT = '%Y-%m-%dT%H:%M'

places = []
times = []

demand_times = []

capacity = 0
max_delay = None


def reset_global_variables():
    global places, times, demand_times, capacity, max_delay

    places = []
    times = []
    demand_times = []
    capacity = None
    max_delay = None


def duration(n1, n2):
    return times[n1][n2]


def calculate_arrival_time(route):
    arrival_times = []
    arrival_times.insert(0, route['times'][0])

    for i in range(1, len(route['nodes'])):

        if route['nodes'][i] == 0 and i < len(route['nodes']) - 1:
            arrival_times.insert(i, route['times'][i + 1] - duration(route['nodes'][i], route['nodes'][i + 1]))
        else:
            arrival_times.insert(i, arrival_times[i - 1] + duration(route['nodes'][i - 1], route['nodes'][i]))

    return arrival_times


def add_depot_nodes(route):
    start_time = route['times'][0] - duration(0, route['nodes'][0])
    route['nodes'].insert(0, 0)
    route['times'].insert(0, start_time)

    i = 1
    while i + 1 < len(route['nodes']):

        if route['nodes'][i + 1] == 0:
            route['nodes'].insert(i + 1, 0)
            arrival_time = route['times'][i] + duration(route['nodes'][i], 0)
            route['times'].insert(i + 1, arrival_time)
            i += 2
        i += 1

    end_time = route['times'][len(route['nodes']) - 1] + duration(route['nodes'][len(route['nodes']) - 1], 0)
    route['nodes'].insert(len(route['nodes']), 0)
    route['times'].insert(len(route['times']), end_time)

    return route


def fitness(route):
    arrival_time = calculate_arrival_time(route)
    nodes = route['nodes']
    cost = duration(0, nodes[0])

    for i in range(len(nodes) - 1):
        previously_point = nodes[i]
        next_point = nodes[i + 1]
        time_difference = 0 if nodes[i] == 0 else abs(demand_times[previously_point] - arrival_time[i])
        cost += duration(previously_point, next_point) + time_difference

    cost += duration(nodes[len(nodes) - 1], 0)
    return cost


def adjust(route):
    repeated = True
    while repeated:
        repeated = False
        for i1 in range(len(route['nodes'])):
            for i2 in range(i1):
                if route['nodes'][i1] == route['nodes'][i2]:
                    have_all = True
                    for nodeId in range(len(places)):
                        if nodeId not in route['nodes']:
                            route['nodes'][i1] = nodeId
                            route['times'][i1] = random.randint(0, 1440)
                            have_all = False
                            break
                    if have_all:
                        del route['nodes'][i1]
                        del route['times'][i1]
                    repeated = True
                if repeated:
                    break
            if repeated:
                break

    # Adjust capacity exceeded
    i = 0
    s = 0.0
    global capacity
    cap = capacity
    while i < len(route['nodes']):
        s += places[route['nodes'][i]]['demand']
        if s > cap:
            route['nodes'].insert(i, 0)
            route['times'].insert(i, 0)
            s = 0.0
        i += 1

    # Adjust two consecutive depots
    i = len(route['nodes']) - 2
    while i >= 0:
        if route['nodes'][i] == 0 and route['nodes'][i + 1] == 0:
            del route['nodes'][i]
            del route['times'][i]
        i -= 1

    # Recalculate arrival time to depot
    route['times'] = calculate_arrival_time(route)
    route['times'][0] = demand_times[route['nodes'][0]]
    i = 1
    while i < len(route['times']):

        if route['nodes'][i] == 0 and i + 1 < len(route['times']):
            route['times'][i + 1] = demand_times[route['nodes'][i + 1]]
        i += 1

    # Add optional 0 (return to depot) if time_difference is greater than max_delay
    route['times'] = calculate_arrival_time(route)
    i = 2
    while i < len(route['times']):
        time_difference = abs(demand_times[route['nodes'][i]] - route['times'][i])
        if route['nodes'][i] != 0 and route['nodes'][i - 1] != 0 and time_difference > max_delay:
            route['nodes'].insert(i, 0)
            route['times'].insert(i, 0)
            route['times'][i + 1] = demand_times[route['nodes'][i + 1]]
            route['times'] = calculate_arrival_time(route)
        i += 1


def run(request_data, durations_matrix):
    global times, places, demand_times, capacity, max_delay
    reset_global_variables()

    times = durations_matrix

    capacity = int(request_data['source']['capacity'])
    number_of_iterations = int(request_data['source']['number_of_iterations'])
    size_of_population = int(request_data['source']['population_size'])
    max_delay = int(request_data['source']['maximum_delay'])

    places.append({
        'address': request_data['source']['address'],
        'demand': 0
    })

    demand_times.append(0)
    for place in request_data['orders']:
        places.append({
            'address': place['address'],
            'demand_time': str(place['demand_time']),
            'demand': int(place['demand'])
        })
        timestamp_as_long = datetime.strptime(str(place['demand_time']), ISO_DATE_FORMAT).timestamp()
        demand_times.append(timestamp_as_long / 60)

    pop = []

    for _ in range(size_of_population):
        nodes = list(range(1, len(places)))
        random.shuffle(nodes)
        route = {'nodes': nodes, 'times': random.sample(list(range(0, 1440)), len(places) - 1)}
        pop.append(route)
    for p in pop:
        adjust(p)

    for i in range(number_of_iterations):
        next_population = []
        parents = []

        for j in range(int(len(pop))):
            parents_ids = set()
            while len(parents_ids) < 4:
                parents_ids |= {random.randint(0, len(pop) - 1)}
            parents_ids = list(parents_ids)

            parent1 = pop[parents_ids[0]] if fitness(pop[parents_ids[0]]) < fitness(pop[parents_ids[1]]) else pop[
                parents_ids[1]]
            parent2 = pop[parents_ids[2]] if fitness(pop[parents_ids[2]]) < fitness(pop[parents_ids[3]]) else pop[
                parents_ids[3]]
            parents.append(parent1 if fitness(parent1) < fitness(parent2) else parent2)

        j = 0
        while j < len(parents) - 1:
            min_random_range = min(len(parents[j]['nodes']), len(parents[j + 1]['nodes'])) - 1
            max_random_range = min(len(parents[j]['nodes']), len(parents[j + 1]['nodes'])) - 1

            left_idx, right_idx = random.randint(1, min_random_range), random.randint(1, max_random_range)
            left_idx, right_idx = min(left_idx, right_idx), max(left_idx, right_idx)

            child_1 = {
                'nodes': (parents[j]['nodes'][:left_idx]
                          + parents[j + 1]['nodes'][left_idx:right_idx]
                          + parents[j]['nodes'][right_idx:]),
                'times': (parents[j]['times'][:left_idx]
                          + parents[j + 1]['times'][left_idx:right_idx]
                          + parents[j]['times'][right_idx:])}

            child_2 = {'nodes': (parents[j + 1]['nodes'][:left_idx]
                                 + parents[j]['nodes'][left_idx:right_idx]
                                 + parents[j + 1]['nodes'][right_idx:]),
                       'times': (parents[j + 1]['times'][:left_idx]
                                 + parents[j]['times'][left_idx:right_idx]
                                 + parents[j + 1]['times'][right_idx:])}

            next_population.extend([child_1, child_2])
            j = j + 2

        parents.clear()

        if random.randint(0, 100) == 1:
            # population_to_mutate
            p_to_mutate = next_population[random.randint(0, len(next_population) - 1)]
            i1 = random.randint(0, len(p_to_mutate['nodes']) - 1)
            i2 = random.randint(0, len(p_to_mutate['nodes']) - 1)
            p_to_mutate['nodes'][i1], p_to_mutate['nodes'][i2] = p_to_mutate['nodes'][i2], p_to_mutate['nodes'][i1]
            p_to_mutate['times'][i1], p_to_mutate['times'][i2] = p_to_mutate['times'][i2], p_to_mutate['times'][i1]

        for p in next_population:
            adjust(p)

        pop = next_population

    better = None
    bf = float('inf')
    for p in pop:
        f = fitness(p)
        if f < bf:
            bf = f
            better = p

    better = add_depot_nodes(better)

    solution = []
    k = 0
    i = 1
    while i < len(better['nodes']):
        if better['nodes'][i] == 0:
            solution.append({'nodes': better['nodes'][k:i + 1], 'times': better['times'][k:i + 1]})
            i += 1
            k = i
        i += 1

    sorted_solution = sorted(solution, key=lambda sol: sol['times'][0])

    json_response = []

    for single_route in sorted_solution:
        json_single_element = []
        for i in range(len(single_route['nodes'])):
            node_id = single_route['nodes'][i]
            json_single_element.append({
                'address': places[single_route['nodes'][i]]['address'],
                'demand_time': None if node_id == 0 else places[node_id]['demand_time'],
                'demand': None if node_id == 0 else places[node_id]['demand'],
                'arrival_time': datetime.fromtimestamp(single_route['times'][i] * 60).strftime(ISO_DATE_FORMAT)
            })
        json_response.append(json_single_element)

    vehicle_routes = [[json_response[0]]]
    for i in range(1, len(json_response)):
        was_added = False
        for vehicle_route in vehicle_routes:
            if json_response[i][0]['arrival_time'] > vehicle_route[-1][-1]['arrival_time']:
                vehicle_route.append(json_response[i])
                was_added = True
                break
        if not was_added:
            vehicle_routes.append([json_response[i]])

    json_to_return = {'routes': json_response, 'vehicle_routes': vehicle_routes}

    reset_global_variables()
    return json_to_return
