import json
from datetime import datetime

from api.models import Order, Route


class CustomJsonEncoder(json.JSONEncoder):

    def default(self, object):

        if isinstance(object, Order) or isinstance(object, Route):
            return object.__dict__
        elif isinstance(object, datetime):
            return str(object)
        else:
            return json.JSONEncoder.default(self, object)
