import requests as req
import os


class OsrmClient:
    osrm_base_url = "http://osrm:5000" if os.getenv("DOCKER_PROFILE") else "http://localhost:5000"

    def get_square_durations_matrix(self, places):
        coordinates_arr = ["{},{}".format(place["address"]["lon"], place["address"]["lat"]) for place in places]
        places_string = ";".join(coordinates_arr)
        request_url = "{}/table/v1/car/{}?scale_factor={}".format(self.osrm_base_url, places_string, 1 / 60)
        response = req.get(request_url)

        return response.json()['durations'] if response.status_code == 200 else []
