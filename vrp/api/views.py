import api.utils.cvrptw as vrp
from api.utils.osrm_client import OsrmClient
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
import json


class VrpSolver(APIView):
    @csrf_exempt
    def post(self, request):
        places = self.__create_places_array_from_request(request)
        durations_matrix = OsrmClient().get_square_durations_matrix(places)

        if not durations_matrix:
            return HttpResponse(status=400)

        response = vrp.run(request.data, durations_matrix)
        return HttpResponse(json.dumps(response), content_type="application/json")

    def __create_places_array_from_request(self, request):
        return ([{"address": request.data["source"]["address"]}]
                + [{"address": order["address"]} for order in request.data["orders"]])
