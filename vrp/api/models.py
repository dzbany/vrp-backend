class Order(object):
    def __init__(self, address, demand_time, arrival_time):
        self.address = address
        self.demand_time = demand_time
        self.arrival_time = arrival_time


class Route(object):
    def __init__(self, vehicle_id, orders):
        self.vehicle_id = vehicle_id
        self.orders = orders
