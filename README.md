# About project

This app is designed and implemented for solve vehicle routing problem with capacity and time windows constraints. 
Uses genetic algorithm to find optimally solution for routing between entered points and maintaining capacity of vehicle and customer order time.

## API DOC

POST ```http://localhost:8080/api/vrp-solver```

Example request body:
```json
{
    "source": {
        "address": {
            "display_name": "Katowice",
            "lat": 50.48374,
            "lon": 18.95136
        },
        "capacity": 10,
        "maximum_delay": 10,
        "number_of_iterations": 200,
        "population_size": 50
    },
    "orders": [
        {
            "address": {
                "display_name": "Nakielska, Osiedle Marii Rozpłochowskiej, Lasowice, Tarnowskie Góry, Tarnowskie Góry County, Górnośląsko-Zagłębiowska Metropolia, Silesian Voivodeship, 42-600, Poland",
                "lat": 50.4424455,
                "lon": 18.8782809
            },
            "demand": 1,
            "demand_time": "2020-01-04T17:30"
        },
        {
            "address": {
                "display_name": "Opolska, Lyszcze, Sowice, Śródmieście – Centrum, Tarnowskie Góry, Tarnowskie Góry County, Górnośląsko-Zagłębiowska Metropolia, Silesian Voivodeship, 42-600, Poland",
                "lat": 50.4472427,
                "lon": 18.8475413
            },
            "demand": 1,
            "demand_time": "2020-01-01T15:00"
        },
        {
            "address": {
                "display_name": "Gliwicka, Osiedle Wieszowa, Świętoszowice-Wieś, Ziemięcice, gmina Zbrosławice, Tarnowskie Góry County, Górnośląsko-Zagłębiowska Metropolia, Silesian Voivodeship, 42-675, Poland",
                "lat": 50.3684805,
                "lon": 18.7183723
            },
            "demand": 4,
            "demand_time": "2020-01-01T12:00"
        }
    ]
}
```

Example response body:
```json
{
    "routes": [
        [
            {
                "address": {
                    "display_name": "Katowice",
                    "lat": 50.48374,
                    "lon": 18.95136
                },
                "demand_time": null,
                "demand": null,
                "arrival_time": "2020-01-01T16:47"
            },
            {
                "address": {
                    "display_name": "Opolska, Lyszcze, Sowice, Śródmieście – Centrum, Tarnowskie Góry, Tarnowskie Góry County, Górnośląsko-Zagłębiowska Metropolia, Silesian Voivodeship, 42-600, Poland",
                    "lat": 50.4472427,
                    "lon": 18.8475413
                },
                "demand_time": "2020-01-01T17:00",
                "demand": 1,
                "arrival_time": "2020-01-01T17:00"
            },
            {
                "address": {
                    "display_name": "Gliwicka, Osiedle Wieszowa, Świętoszowice-Wieś, Ziemięcice, gmina Zbrosławice, Tarnowskie Góry County, Górnośląsko-Zagłębiowska Metropolia, Silesian Voivodeship, 42-675, Poland",
                    "lat": 50.3684805,
                    "lon": 18.7183723
                },
                "demand_time": "2020-01-01T17:00",
                "demand": 4,
                "arrival_time": "2020-01-01T17:23"
            },
            {
                "address": {
                    "display_name": "Katowice",
                    "lat": 50.48374,
                    "lon": 18.95136
                },
                "demand_time": null,
                "demand": null,
                "arrival_time": "2020-01-01T17:55"
            }
        ]
    ],
    "vehicle_routes": [
        [
            [
                {
                    "address": {
                        "display_name": "Katowice",
                        "lat": 50.48374,
                        "lon": 18.95136
                    },
                    "demand_time": null,
                    "demand": null,
                    "arrival_time": "2020-01-01T16:47"
                },
                {
                    "address": {
                        "display_name": "Opolska, Lyszcze, Sowice, Śródmieście – Centrum, Tarnowskie Góry, Tarnowskie Góry County, Górnośląsko-Zagłębiowska Metropolia, Silesian Voivodeship, 42-600, Poland",
                        "lat": 50.4472427,
                        "lon": 18.8475413
                    },
                    "demand_time": "2020-01-01T17:00",
                    "demand": 1,
                    "arrival_time": "2020-01-01T17:00"
                },
                {
                    "address": {
                        "display_name": "Gliwicka, Osiedle Wieszowa, Świętoszowice-Wieś, Ziemięcice, gmina Zbrosławice, Tarnowskie Góry County, Górnośląsko-Zagłębiowska Metropolia, Silesian Voivodeship, 42-675, Poland",
                        "lat": 50.3684805,
                        "lon": 18.7183723
                    },
                    "demand_time": "2020-01-01T17:00",
                    "demand": 4,
                    "arrival_time": "2020-01-01T17:23"
                },
                {
                    "address": {
                        "display_name": "Katowice",
                        "lat": 50.48374,
                        "lon": 18.95136
                    },
                    "demand_time": null,
                    "demand": null,
                    "arrival_time": "2020-01-01T17:55"
                }
            ]
        ]
    ]
}
```

## How to run

#### Prerequisites
1. Install [Docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/) tools.
2. Get suitable Open Street Map data from Geofabrik and unpack it into data directory. 
   For Poland OSM run following commands:
   - `wget http://download.geofabrik.de/europe/germany/poland-latest.osm.pbf`
   - `docker run -t -v "${PWD}:/data" osrm/osrm-backend osrm-extract -p /opt/car.lua /data/poland-latest.osm.pbf`
   - `docker run -t -v "${PWD}:/data" osrm/osrm-backend osrm-partition /data/poland-latest.osrm`
   - `docker run -t -v "${PWD}:/data" osrm/osrm-backend osrm-customize /data/poland-latest.osrm`

#### Run app 

Use ````docker-compose up -d```` command to run app (Frontend, Backend and OSRM microservices) locally and then visit browser at http://localhost:8080